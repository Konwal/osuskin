This is my own osu! skin i based this on [Tokaku tutorial](url) also I added some assets form one of [Rafis skins](https://gist.github.com/vistafan12/c131048fa696f651a4deb998b77dfe95) (I don't remember which one I used). You can check my osu! [profile](https://osu.ppy.sh/users/22330692).

# Screenshots
![Menu](Menu.png)

![Gameplay](Gameplay.png)
